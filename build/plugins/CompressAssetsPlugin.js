const JSZip = require('jszip');
const { RawSource } = require('webpack-sources')


const pluginName = 'CompressAssetsPlugin';

class CompressAssetsPlugin {
    constructor({output}){
        this.output = output;
    }

    apply(compiler){
        compiler.hooks.emit.tapAsync(pluginName, (compilation, callback)=> {
            
            const zip = new JSZip();
            const assets = compilation.getAssets();
            assets.forEach((item) => {
                const {name, source} = item;
                const sourceCode = source.source();
                zip.file(name, sourceCode)
            })

            zip.generateAsync({ type: 'nodebuffer' }).then((result) => {
                // 通过 new RawSource 创建压缩包
                // 并且同时通过 compilation.emitAsset 方法将生成的 Zip 压缩包输出到 this.output
                compilation.emitAsset(this.output, new RawSource(result));
                // 调用 callback 表示本次事件函数结束
                callback();
              });
            // console.log('---compressAssetsPlugin---', assets.length)
        })
    }
}

module.exports = CompressAssetsPlugin;
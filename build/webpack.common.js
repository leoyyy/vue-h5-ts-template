const path = require('path');
const dotenv = require('dotenv');
const DotenvWebpack = require('dotenv-webpack');
// eslint-disable-next-line import/no-extraneous-dependencies
const chalk = require('chalk'); // 修改控制台字符样式
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { VueLoaderPlugin } = require('vue-loader'); // vue加载器
const ESLintPlugin = require('eslint-webpack-plugin');
// const ExternalsWebpackPlugin = require('./plugins/externals-webpack-plugin');

const isProd = process.env.CURRENT_ENV === 'pro'; // cross-env全局变量

const envConfigPath = {
    dev: path.resolve(__dirname, '../env/.env.dev'), // 开发环境配置
    test: path.resolve(__dirname, '../env/.env.test'), // 测试环境配置
    prod: path.resolve(__dirname, '../env/.env.pro'), // 正式环境配置
};

dotenv.config({
    path: envConfigPath[process.env.CURRENT_ENV], // 配置文件路径
    encoding: 'utf8', // 编码方式，默认utf8
    debug: false, // 是否开启debug，默认false
});

const cssConfig = [
    isProd ? {
        loader: MiniCssExtractPlugin.loader,
        options: {
            publicPath: '../../',
        },
    } : 'vue-style-loader',
    'css-loader', { // 从右向左解析
        loader: 'postcss-loader',
        options: {
            postcssOptions: {
                plugins: [
                    'autoprefixer',
                ],
            },
        },
    }, 'sass-loader',
];

module.exports = {
    entry: path.resolve(__dirname, '../src/main.ts'), // 入口文件
    // 只能有一个出口
    output: {
        path: path.resolve(__dirname, '../dist'),
        filename: '[name].[contenthash:8].js',
        clean: true, // 每次构建清除dist包
    },
    resolve: {
        extensions: ['.js', '.jsx', '.json', '.vue', '.ts'], // 省略文件后缀
        alias: { // 配置别名
            '@': path.resolve(__dirname, '../src'),
        },
    },
    // 防止将外部资源包打包到自己的bundle中
    externals: {
        jquery: 'jQuery',
        // vue: 'Vue'
    },
    stats: 'errors-only',
    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, '../public/index.html'),
            filename: 'index.html',
        }),
        // 进度条
        new ProgressBarPlugin({
            format: ` :msg [:bar] ${chalk.green.bold(':percent')} (:elapsed s)`,
        }),
        new MiniCssExtractPlugin({
            filename: 'static/css/[name].css',
        }),
        new VueLoaderPlugin(),
        // new eslintWebpackPlugin(),
        // eslint保存验证
        // new ESLintPlugin({
        //     fix: false, /* 自动帮助修复 */
        //     extensions: ['js', 'json', 'coffee', 'vue'],
        //     exclude: 'node_modules',
        // }),
        new DotenvWebpack({
            // path: path.resolve(__dirname, '../.env'), // 配置文件路径
            path: envConfigPath[process.env.CURRENT_ENV], // 根据环境配置文件路径
        }),
    ],
    module: {
        rules: [{
            test: /\.vue$/,
            loader: 'vue-loader',
            include: [path.resolve(__dirname, '../src')],
        }, {
            test: /\.(css|scss|sass)$/,
            // css-loader 处理import
            // style-loader css注入到js
            use: cssConfig, // 从右向左解析
        }, {
            test: /.(png|svg|jpg|jpeg|gif|eot|svg|ttf|woff|woff2)$/i,
            type: 'javascript/auto',
            use: [
                {
                    loader: 'url-loader',
                    options: {
                        limit: 1024 * 100,
                        name: 'assets/images/[name].[hash].[ext]',
                        esModule: false,
                    },
                },
            ],
        }, {
            test: /\.html$/i,
            loader: 'html-loader',
            options: {
                esModule: !!isProd,
            },
        }, {
            test: /\.(t|j)s$/,
            exclude: /node_modules/,
            use: [
                {
                    loader: 'ts-loader',
                    options: {
                    // 指定特定的ts编译配置，为了区分脚本的ts配置
                        configFile: path.resolve(__dirname, '../tsconfig.json'),
                        // 对应文件添加个.ts或.tsx后缀
                        appendTsSuffixTo: [/\.vue$/],
                        transpileOnly: true, // 关闭类型检测，即值进行转译
                    },
                },
            ],
        }],
    },
};

module.exports = {
    env: {
        browser: true,
        es6: true,
        node: true,
    },
    extends: [
        'plugin:@typescript-eslint/recommended',
        'eslint:recommended',
        'plugin:vue/vue3-essential',
        'airbnb-base',
    ],
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly',
    },
    parserOptions: {
        // parser: 'vue-eslint-parser',
        ecmaVersion: 2018,
        parser: '@typescript-eslint/parser',
        sourceType: 'module',
    },
    parser: 'vue-eslint-parser',
    plugins: [
        'vue',
        '@typescript-eslint',
    ],
    settings: {
        'import/extensions': ['.js', '.jsx', '.ts', '.tsx'],
        'import/parsers': {
            '@typescript-eslint/parser': ['.ts', '.tsx'],
        },
    },
    rules: {
        '@typescript-eslint/no-explicit-any': 'off',
        '@typescript-eslint/ban-types': 'off',
        // '@typescript-eslint/no-use-before-define': 'off',
        'global-require': 'off',
        'prefer-template': 0,
        indent: ['error', 4],
        'linebreak-style': ['off', 'windows'],
        'no-console': 'off',
        'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
        'import/export': 2,
        'no-unused-vars': 'off',
    },
};

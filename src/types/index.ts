export interface getHomeListParams {
    currentPage: number,
    pageSize: number
}

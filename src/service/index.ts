import hfetch from './hfetch';

import { getHomeListParams } from '../types';

export default {
    getHomeDetail: () => hfetch.get('/b2c-mall/v1/home/getHomeDetail'),
    getHomeSkuListRecommend: (params: getHomeListParams) => hfetch.post('/b2c-mall/v1/home/getHomeSkuListRecommend', params),
};

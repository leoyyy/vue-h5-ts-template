import axios, { AxiosResponse, AxiosRequestConfig, AxiosError } from 'axios';

const baseURL = process.env.API_HOST;
const requestTimeout = 10000;

const instance = axios.create({
    baseURL,
    timeout: requestTimeout,
});

instance.interceptors.request.use((config: AxiosRequestConfig) => {
    config.headers['X-Client-Id'] = 'mini-app';
    return config;
}, (error: AxiosError) => Promise.reject(error));

// eslint-disable-next-line max-len
instance.interceptors.response.use((res: AxiosResponse) => {
    console.log('res', res.data);
    return Promise.resolve(res.data);
}, (error: AxiosError) => Promise.reject(error));

export default instance;

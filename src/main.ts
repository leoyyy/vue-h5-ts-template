import Vue, { createApp } from 'vue';
import './assets/styles/index.scss';
// import $ from 'jquery'

import router from './router';

import App from './App.vue';

const app = createApp(App);

app.use(router).mount('#app');

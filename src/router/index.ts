import { createWebHistory, createRouter } from 'vue-router';
import routes from './routePath';

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;

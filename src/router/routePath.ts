import { RouteRecordRaw } from 'vue-router';

const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        name: 'home',
        component: import(/* webpackChunkName: "home" */ '../pages/home/index.vue'),
    }, {
        path: '/mine',
        name: 'mine',
        component: import('../pages/mine/index.vue'),
    },
];

export default routes;
